package com.kuubstudios.lambchops;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemFood;

/**
 * Lambchops
 * 
 * ItemLambchop
 * 
 * @author Goz3rr
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

public class ItemLambchop extends ItemFood {

	public ItemLambchop(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);		
	}

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister iconRegister) {
    	itemIcon = iconRegister.registerIcon("lambchops:" + this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
    }
}
