package com.kuubstudios.lambchops;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * Lambchops
 * 
 * Lambchops
 * 
 * @author Goz3rr
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

@Mod(modid = "lambchops", name = "LambChops Mod", version = "0.3.2 For MC1.5.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class LambChops
{
    //private final static Item LambChopsItemRaw = new LambChopItemRaw(13927);
    private static Item LambChopRaw;
    private static Item LambChopCooked;
    static int LambChopRawID;
    static int LambChopCookedID;

    @Instance("lambchops")
    public static LambChops instance;

    @SidedProxy(clientSide = "com.kuubstudios.lambchops.LambChopsClientProxy", serverSide = "com.kuubstudios.lambchops.LambChopsCommonProxy")
    public static LambChopsCommonProxy proxy;

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(new LambChops());
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        LambChopRawID = config.getItem("LambChopRawID", 13927).getInt();
        LambChopCookedID = config.getItem("LambChopCookedID", 13928).getInt();
        config.save();
    }

    @Init
    public void load(FMLInitializationEvent event)
    {
        LambChopRaw = new ItemLambchop(LambChopRawID, 2, 0.2F, true).setUnlocalizedName("lambchopRaw");
        LambChopCooked = new ItemLambchop(LambChopCookedID, 6, 0.6F, true).setUnlocalizedName("lambchopCooked");
        LanguageRegistry.addName(LambChopRaw, "Raw Lambchop");
        LanguageRegistry.addName(LambChopCooked, "Cooked Lambchop");
        GameRegistry.addSmelting(LambChopRaw.itemID, new ItemStack(LambChopCooked), 0.5f);
    }

    @ForgeSubscribe
    public void dropItem(LivingDropsEvent event)
    {
        Entity src = event.entityLiving;

        if (src instanceof EntitySheep)
        {
            Random rand = new Random();
            int var3 = rand.nextInt(2) + 1 + rand.nextInt(1 + event.lootingLevel);

            for (int i = 0; i < var3; i++)
            {
                if (src.isBurning())
                {
                    src.dropItem(LambChopCooked.itemID, 1);
                }
                else
                {
                    src.dropItem(LambChopRaw.itemID, 1);
                }
            }
        }
    }
}
